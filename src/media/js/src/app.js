/*!
 * App
 * [Date here]
 * Sborka Project
 */

// Note: Use 'search & replace' to rename 'App' to current project name an delete this note
var App = new (function App() {

    this.dom = {
        $window: $(window),
        $document: $(document)
    };

    this.env = {
        mobileMode: /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)
    };

    this.modules = {};
    this.utils = {};

    // Base inits here:
    CSSPlugin.defaultTransformPerspective = 300;

    // DOM-Ready inits:
    var self = this;
    $(function() {
        self.dom.$body = $('body');
        self.dom.$html = $('html');

        // Modules init here
        self.modules.SVGSprites.init();
    });
})();

App.modules.SVGSprites = {
    init: function() {
        var self = this;
        $.get('media/svg/sprite.svg', function(data) {
            $('<div style="width:0; height:0; overflow:hidden"></div>').prependTo(App.dom.$body).html(self.serializeXml(data.documentElement));
        });
    },
    serializeXml: function(xmldom) {
        if (typeof XMLSerializer != 'undefined') {
            return (new XMLSerializer()).serializeToString(xmldom);
        } else {
            return $(xmldom).html();
        }
    }
};

$(document).ready(function(){

    var gallery = $('.js-photo-gallery');
    if (gallery.length && productImages.length) {
        for (var i = 0; i < productImages.length; i++) {
            if (i >= 8) {
                break;
            }
            var galleryItem = $('<li>').attr('imageId', productImages[i].id);
            galleryItem.append($('<img>', {src: productImages[i].standart}));
            gallery.append(galleryItem);
        }
        $(".js-photo-gallery li").click(changeMainPhoto);

        $(".js-photo-gallery li")[0].click();
    }

    $(".js-scroll-to-top").click(function() {
        $('html, body').animate({ scrollTop: 0 }, 600);
    })
    if ($('.tabs li').length > 0) {
        $('.tabs li').click(onCardTabClicked);
        //open first tab in tabs
        try {
            $('.tabs li')[0].click();
        } catch(err) {
            console.log('cannot open tab, ', err);
        }
    }
    $('.js-quantity-inc').click(onChangeQuantity);
    $('.js-quantity-decr').click(onChangeQuantity);


    // Instantiate EasyZoom instances
    if ($('.easyzoom').length) {
        var $easyzoom = $('.easyzoom').easyZoom();

        // Get an instance API
        var api = $easyzoom.data('easyZoom');       
    }

    $(".dropdown-container").click(onDropdownClick);
    $('.dropdown-list li').click(onDropdownChange);

    var glass = $('.glass');

    $(".js-standart-image").on('mousemove', mouseMove);

    $(".js-standart-image").on('mouseout', function() {
        glass.fadeOut();
        glass.off('mousemove', mouseMove);
    });
    $(".js-standart-image").on('mouseenter', function() {
        glass.fadeIn();
        glass.on('mousemove', mouseMove);
    });

    $(".info-tooltip-trigger").on('click', function(event) {
        event.stopPropagation();
        event.preventDefault();
        var tooltipX = event.pageX;
        var tooltipY = event.pageY;
        var windowWidth = $(window).width();
        var documentHeight = $(document).height();
        if (tooltipX + 300 > windowWidth) {
            tooltipX = windowWidth - 320;
            if (windowWidth > 350) {
                $('.tooltip').addClass('right-arrow');
            } else {
                $('.tooltip').removeClass('right-arrow');
            }
        } else {
            $('.tooltip').removeClass('right-arrow');
            tooltipX = tooltipX - 35;
        }
        if (tooltipY + 200 > documentHeight) {
            tooltipY = tooltipY - 210;
            $('.tooltip').addClass('bottom-arrow');
        } else {
            $('.tooltip').removeClass('bottom-arrow');
            tooltipY = tooltipY + 30;
        }
        
        
        $('.tooltip').css('left', tooltipX);
        $('.tooltip').css('top', tooltipY);
        $('.tooltip').fadeIn();

        $(document).one('click', function(e) {
            $('.tooltip').fadeOut();
        });
    });


    $(".product-photos__upload").change(function(){
        $(this).closest('.product-photos__upload-wrapper').removeClass('image-chosen').addClass('image-chosen');
        readURL(this);
    });

    $(".js-select-category-container .simple-block").on('click', function(e){
        var containerBlock = $(e.target).closest('.simple-block');
        var cb = containerBlock.find('input[type="checkbox"]');
        if (cb.is(':checked')) {
            cb.prop('checked', false);
            containerBlock.removeClass('active');
        } else {
            cb.prop('checked', true);
            containerBlock.addClass('active');
        }

    })
});

function readURL(input) {
    if (input.files && input.files[0]) {

        var resultContainer = $(input).closest('.product-photos__upload-wrapper');
        if (resultContainer.length !== 0) {
            for (var i = 0; i < input.files.length; i++) {
                var reader = new FileReader();
                reader.readAsDataURL(input.files[i]);
                reader.onload = function (e) {
                    var image = $('<div class="product-photos__uploaded"></div>')
                        .append($('<img />')
                            .attr('src', e.target.result)
                            );
                    image.append('<svg class="icon" viewBox="0 0 25 25"><use xlink:href="#icon-close"></use></svg>')
                        
                    $(resultContainer).before(image);
                };
            }
        }
    }
}
function onDropdownChange(e) {
    e.preventDefault();
    e.stopPropagation();
    var ddContainer = $(e.target).closest('.dropdown-container');
    var selected = $(e.target).closest('li');
    var selectedValue = selected.html();
    if (selected.find('.categories-icon').length !== 0) {
        selectedValue = selected.text();
    }
    ddContainer.find('.dropdown-selected-value').html(selectedValue);

    $('.dropdown-list').removeClass('opened');
    $(document).off('click');
}
function onDropdownClick(e) {
    $('.dropdown-list').removeClass('opened');
    var ddContainer = $(e.target).closest('.dropdown-container');
    ddContainer.find('.dropdown-list').addClass('opened');

    $(document).on('click', function(e) {
        if (!$(e.target).closest('.dropdown-container').length) {
            $('.dropdown-list').removeClass('opened');
            $(document).off('click');
        } 
    })
}
function mouseMove(e) {
    var glass = $('.glass');
    var photoPos = $('.js-standart-image').offset();
    var containerPos = $('.easyzoom').offset();
    var glassDefaultSize = 200;

    var glass_left = e.pageX - glassDefaultSize / 2;
    var glass_top  = e.pageY - glassDefaultSize / 2;

    if (glass_top < photoPos.top) {
        glass_top = photoPos.top;
    }
    if (glass_left < photoPos.left) {
        glass_left = photoPos.left;
    }
    var photoW = $('.js-standart-image').width();
    var photoH = $('.js-standart-image').height();
    if (glass_left + glassDefaultSize - photoPos.left > photoW) {
        glass_left = photoPos.left + photoW - 4 - glassDefaultSize;
        //glass.width(photoW - glass_left + photoPos.left);
    }
    if (glass_top + glassDefaultSize - photoPos.top > photoH) {
        glass_top = photoPos.top + photoH - 4 - glassDefaultSize;
        //glass.height(photoH - glass_top + photoPos.top);
    }

    glass.css({
      left: glass_left,
      top: glass_top
    });
  };


function changeMainPhoto(evt) {
    var galleryItem = $(evt.target).closest('li');
    var imageId = parseInt(galleryItem.attr('imageId'));
    if (isNaN(imageId)) {
        return;
    }
    for (var i = 0; i < productImages.length; i++) {
        if (productImages[i].id === imageId) {
            var $easyzoom = $('.easyzoom').easyZoom();
            var easyzoomApi = $easyzoom.data('easyZoom');
            easyzoomApi.swap(productImages[i].standart, productImages[i].zoom);
            $(".js-photo-gallery li").removeClass('active');
            galleryItem.addClass('active');
            return;
        }
    } 
}

function onCardTabClicked(evt) {
    evt.preventDefault();
    evt.stopPropagation();
    $(evt.target).closest('.tabs').find('.active').removeClass('active');
    //because we can have more than one tablinks on our page
    $(evt.target).closest('li').addClass('active');

    $('.tabs-content').find('.tab-content').removeClass('active');
    var blockId = $(evt.target).closest('li')[0].id + '-block';
    $('#' + blockId).addClass('active');
}
function onChangeQuantity(evt) {
    var quantity = parseInt($('.js-quantity').text());
    if (isNaN(quantity)) {
        return;
    }
    if ($(evt.target).closest('svg').classList.value.indexOf('js-quantity-inc') !== -1) {
        quantity ++;
    } else {
        quantity --;
    }
    if (quantity < 0) {
        quantity = 0;
    }
    $('.js-quantity').text(quantity);
}

const productImages = [
    {
        'id': 123,
        'standart': "media/img/1_standard.jpg",
        'zoom': "media/img/1_zoom.jpg",
    },
    {
        'id': 129783,
        'standart': "media/img/2_standard.jpg",
        'zoom': "media/img/2_zoom.jpg",
    },
    {
        'id': 543,
        'standart': "media/img/product.jpg",
        'zoom': "media/img/product-big.jpg",
    },
    {
        'id': 1293,
        'standart': "media/img/1_standard.jpg",
        'zoom': "media/img/1_zoom.jpg",
    },
    {
        'id': 83,
        'standart': "media/img/2_standard.jpg",
        'zoom': "media/img/2_zoom.jpg",
    },
    {
        'id': 3,
        'standart': "media/img/product.jpg",
        'zoom': "media/img/product-big.jpg",
    },
    {
        'id': 1267,
        'standart': "media/img/1_standard.jpg",
        'zoom': "media/img/1_zoom.jpg",
    },
    {
        'id': 183,
        'standart': "media/img/2_standard.jpg",
        'zoom': "media/img/2_zoom.jpg",
    },
    {
        'id': 5403,
        'standart': "media/img/product.jpg",
        'zoom': "media/img/product-big.jpg",
    }
];
